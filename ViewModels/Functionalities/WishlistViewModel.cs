﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabProject.ViewModels
{
    public class WishlistViewModel
    {
        public string WishlistName { get; set; }
        public string WishlistAuthor { get; set; }
        public string WishlistComment { get; set; }
        public string AdditionalNameIfExists { get; set; }
    }
}
