﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabProject.ViewModels.Product
{
    public class FilterProductsViewModel
    {
        public string FirstCriteriaFilter { get; set; }
        public int FirstCriteriaItemNo { get; set; }
        public string SecondCriteriaFilter { get; set; }
        public int SecondCriteriaItemNo { get; set; }
        public string ThirdCriteriaFilter { get; set; }
        public int ThirdCriteriaItemNo { get; set; }
        public string FourthCriteriaFilter { get; set; }
        public int FourthCriteriaItemNo { get; set; }
    }
}
