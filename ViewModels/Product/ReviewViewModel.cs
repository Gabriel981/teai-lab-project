﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabProject.ViewModels
{
    public class ReviewViewModel
    {
        public string CompleteName { get; set; }
        public string Email { get; set; }
        public int NoOfStars { get; set; }
        public string Comment { get; set; }
    }
}
