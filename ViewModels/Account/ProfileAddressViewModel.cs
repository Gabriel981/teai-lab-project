﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabProject.ViewModels
{
    public class ProfileAddressViewModel
    {
        public string Address { get; set; }
        public string Judet { get; set; }
        public string Localitate { get; set; }
        public string PostalCode { get; set; }
    }
}
