﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabProject.ViewModels
{
    public class QuestionViewModel
    {
        public string QuestionType { get; set; }
        public string CompleteName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string QuestionMessage { get; set; }
    }
}
