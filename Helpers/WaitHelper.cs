﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace LabProject.Helpers
{
    public static class WaitHelper
    {
        public static void WaitForElementVisibility(IWebDriver webDriver, By byElement, int timeSpan = 30)
        {
            WebDriverWait driverWait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(timeSpan));
           driverWait.Until(ExpectedConditions.ElementIsVisible(byElement));            
        }

        public static void WaitForElementVisibilityV2(IWebDriver webDriver, IWebElement webElement, int timeSpan = 30)
        {
            WebDriverWait driverWait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(timeSpan));
            List<IWebElement> elements = new List<IWebElement>();
            elements.Add(webElement);
            ReadOnlyCollection<IWebElement> readCollection = new ReadOnlyCollection<IWebElement>(elements);

            driverWait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(readCollection));
        }
    }
}
