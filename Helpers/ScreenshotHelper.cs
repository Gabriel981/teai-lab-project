﻿using OpenQA.Selenium;
using System;

namespace LabProject.Helpers
{
    public static class ScreenshotHelper
    {
        public static void TakeScreenshot(IWebDriver webDriver, string screenshotScope)
        {
            var location = @"C:\Users\Gabriel\Desktop\SeleniumScreens\Screen " + screenshotScope;
            var crtTime = DateTime.Now.ToLongDateString();
            var finalLocation = location + " " + crtTime + $".png";
            var screenshotDriver = webDriver as ITakesScreenshot;
            var screenshot = screenshotDriver.GetScreenshot();
            screenshot.SaveAsFile(finalLocation, ScreenshotImageFormat.Png);
        }
    }
}
