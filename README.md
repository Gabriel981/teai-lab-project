# TEAI-Lab-Project

This project contains some methods that are related one to another. In order to run all the tests correctly, some properties needs some changes, and some of them are marked in comments right on [TestMethod]. 

Some methods are provided in two versions, in order to provide a comparison between different methods that can be applied to achieve the same result. One of this methods can be found in Flow tests - [Should_search_product_by_filtersV2], that represents a more accurate alternative to the original filter operations method.
