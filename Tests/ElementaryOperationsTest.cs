﻿using LabProject.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace LabProject.Tests
{
    [TestClass]
    public class ElementaryOperationsTest
    {
        private IWebDriver _webDriver;
        private MainPage _mainPage;

        [TestInitialize]
        public void TestInitialisation()
        {
            _webDriver = new ChromeDriver();
             _webDriver.Manage().Window.Maximize();
            _webDriver.Navigate().GoToUrl("https://cel.ro/");
            _mainPage = new MainPage(_webDriver);
        }

        //Test 1 - ElementaryOperationsTest - Should check if a user can access the product page from the application
        //menus, starting from the main page of the app and ending withing the ProductDetails page
        [TestMethod]
        public void Should_access_product_page()
        {
            var _productsPage = _mainPage.NavigateToProductsPage();
            ProductDetailsPage productDetailsPage = _productsPage.NavigateToProductDetails();

            bool isCompariBtnAvailable = productDetailsPage.VerifyPresence();
            Assert.AreEqual(expected: isCompariBtnAvailable, actual: true);
        }

        //Test 2 - ElementaryOperationsTest - Should check if a user that is not signed-in or registered in the app
        //can add a new product to the basket by navigating though the menus of app
        [TestMethod]
        public void Should_add_product_basket()
        {
            var _productsPage = _mainPage.NavigateToProductsPage();
            var basketPage = _productsPage.NavigateToBasket();

            bool isCartOrderLblAvailable = basketPage.VerifyProductExistence();
            Assert.AreEqual(expected: isCartOrderLblAvailable, actual: true);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _webDriver.Quit();
        }
    }
}
