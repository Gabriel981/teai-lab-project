﻿using LabProject.Helpers;
using LabProject.PageObjects;
using LabProject.PageObjects.Account;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabProject.Tests
{
    [TestClass]
    public class BoundaryValueTests
    {
        private IWebDriver _webDriver;
        private MainPage _mainPage;

        [TestInitialize]
        public void TestInitialization()
        {
            _webDriver = new ChromeDriver();
            _webDriver.Manage().Window.Maximize();
            _webDriver.Navigate().GoToUrl("https://cel.ro");
            _mainPage = new MainPage(_webDriver);
        }

        //Test 1 - BoundaryValueTests - This test should check if the e-mail input from Register Page should accept
        //the e-mail provided below
        [TestMethod]
        public void Should_accept_email_length()
        {
            RegisterPage registerPage = _mainPage.NavigateToRegisterPage();
            registerPage.CheckEmailInputLength("a@mail.com");

            bool noErrorFound = registerPage.ConfirmationError.Count > 0 ? true : false;

            Assert.AreEqual(expected: false, actual: noErrorFound);
        }

        //Test 2 - BoundaryValueTests - This test should check if the e-mail provided below will be accepted or not
        //by CEL.ro for registering a new account (check if an e-mail with less than lower limit can be registered)
        [TestMethod]
        public void Should_not_accept_email_length()
        {
            RegisterPage registerPage = _mainPage.NavigateToRegisterPage();
            registerPage.CheckEmailInputLength("a@a.c");

            WaitHelper.WaitForElementVisibility(_webDriver, registerPage.LblConfirmationError, 50);
            bool notValidEmailLength = registerPage.ConfirmationError[0].Text == "E-mail invalid" ? true : false;

            Assert.AreEqual(true, notValidEmailLength);
        }

        //This test should check the comparison of at least 2 products from the same categories in order to test this functionality of app
        [TestMethod]
        public void Should_accept_products_comparing()
        {
            var productsPage = _mainPage.NavigateToProductsPage();
            var firstProductDetailsPage = productsPage.NavigateToCompareProducts();
            var productsPageBack = firstProductDetailsPage.CompareActionFirstProduct();
            var secondProductDetailsPage = productsPageBack.NavigateToCompareProductTwo();
            var compareProductsPage = secondProductDetailsPage.NavigateToCompareProduct();
            var comparatorPage = compareProductsPage.CompareProducts();

            WaitHelper.WaitForElementVisibility(_webDriver, comparatorPage.LblConfirmationComparation);
            bool productsAreCorrect = compareProductsPage.VerifyNumberOfProducts();

            bool result = false;
            if (productsAreCorrect == true)
            {
                comparatorPage.Confirmation.Displayed.Equals("true");
                result = true;
            }
            else
            {
                comparatorPage.Confirmation.Displayed.Equals("false");
                result = false;
            }
            Assert.AreEqual(true, result);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _webDriver.Quit();
        }

    }
}
