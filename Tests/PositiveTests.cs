﻿using LabProject.Helpers;
using LabProject.PageObjects;
using LabProject.PageObjects.Account;
using LabProject.PageObjects.ContactHelp;
using LabProject.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace LabProject.Tests
{
    [TestClass]
    public class PositiveTests
    {
        private IWebDriver _webDriver;
        private MainPage _mainPage;
        private LoginPage _loginPage;

        [TestInitialize]
        public void TestInitialization()
        {
            _webDriver = new ChromeDriver();
            _webDriver.Manage().Window.Maximize();
            _webDriver.Navigate().GoToUrl("https://cel.ro/");
            _mainPage = new MainPage(_webDriver); 
        }

        //Test 1 - PositiveTests - Should check if two products from same sub-categories/sub-subcategories can be compared

        [TestMethod]
        public void Should_succesfully_compare_two_products()
        {
            var productsPage = _mainPage.NavigateToProductsPage();
            var firstProductDetailsPage = productsPage.NavigateToCompareProducts();
            var productsPageBack = firstProductDetailsPage.CompareActionFirstProduct();
            var secondProductDetailsPage = productsPageBack.NavigateToCompareProductTwo();
            var compareProductsPage = secondProductDetailsPage.NavigateToCompareProduct();
            var comparatorPage = compareProductsPage.CompareProducts();
            
            WaitHelper.WaitForElementVisibility(_webDriver, comparatorPage.LblConfirmationComparation);
            Assert.IsTrue(comparatorPage.Confirmation.Text.ToString().Contains("Compari"));
        }

        //Test2 - PositiveTests - Should access the profile window of the current signed-in user 
        //and add a new address, in order to check this functionality.

        [TestMethod]
        public void Should_add_new_adress_profile()
        {
            _loginPage = new LoginPage(_webDriver);
            LoginViewModel loginView = new LoginViewModel()
            {
                Email = "gabriel.golea18@gmail.com",
                Password = "aFjscIWRDy8c"
            };

            _mainPage = _loginPage.LoginInApplication(loginView);

            UserProfilePage userProfilePage = _mainPage.NavigateToProfile();

            userProfilePage.AddNewAddress(new ProfileAddressViewModel()
            {
                Address = "Str Morilor, nr. 17", 
                Judet = "Bacau",
                Localitate = "Bacau",
                PostalCode = "305101"
            });

            string ConfirmationAddressesText = userProfilePage.ConfirmationAddresses.Text.ToString();
            Assert.AreEqual("Vezi toate adresele", ConfirmationAddressesText);
        }


        //Test3 - PositiveTests - Should check if the profile details can be updated by a signed-in user
        //and also to test some additional functionalities from Profile page (Newsletter - important)
        [TestMethod]
        public void Should_update_profile_details()
        {
            _loginPage = new LoginPage(_webDriver);
            LoginViewModel loginView = new LoginViewModel()
            {
                Email = "gabriel.golea18@gmail.com",
                Password = "aFjscIWRDy8c"
            };

            MainPage mainPage = _loginPage.LoginInApplication(loginView);

            UserProfilePage userProfilePage = mainPage.NavigateToProfile();
            userProfilePage.UpdateProfileDetails(new ProfileDetailsViewModel()
            {
                FirstName = "George",
                LastName = "Vasile",
                Email = "gabriel.golea18@gmail.com",
                PhoneNumber = "0728282715"
            });

            Assert.AreEqual(userProfilePage.ConfirmationProfile.Text.ToString(), "Contul meu");
        }

        //Test 4 - Shoudl check if a new general question category can be sent to the CEL.ro
        //in order to see if a non-registered/non authenticated can contact the team CEL.ro regarding different problems
        [TestMethod]
        public void Should_send_question()
        {
            ContactPage contactPage =  _mainPage.NavigateToContact();
            var replaceQuestionVars = "b";
            QuestionViewModel questionView = new QuestionViewModel()
            {
                CompleteName = $"USER TEST{replaceQuestionVars}",
                Email = $"user.test{replaceQuestionVars}@mail.com",
                QuestionType = "generala",
                PhoneNumber = "0727281817",
                QuestionMessage = $"QUESTION TEST{replaceQuestionVars}"
            };

            contactPage.SendOrderQuestion(questionView);

            Assert.IsTrue(contactPage.TxtConfirmation.Text.ToString().Contains("Mesajul a fost trimis"));
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _webDriver.Quit();
        }
    }
}
