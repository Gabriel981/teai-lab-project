﻿using LabProject.Helpers;
using LabProject.PageObjects;
using LabProject.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace LabProject.Tests
{
    [TestClass]
    public class FlowTests
    {
        private IWebDriver _webDriver;
        private MainPage _mainPage;

        [TestInitialize]
        public void TestInitialisation()
        {
            _webDriver = new ChromeDriver();
            _webDriver.Manage().Window.Maximize();
            _webDriver.Navigate().GoToUrl("https://cel.ro/");

            _mainPage = new MainPage(_webDriver); 
        }

        //Test 1 - FlowTests - Should check if CEL.ro offers the posibilities to select a product or to view a set of products
        //based on some elements like a search-bar (universal in app) and some filter elements (Stock, Price, Seller etc)
        [TestMethod]
        public void Should_search_products_using_filters()
        {
            var productPage = _mainPage.SearchProductBySearchBar("laptop");
            productPage.ApplySearchCriterias("pret");

            string btnResetFiltersText = productPage.BtnResetFilters.Text.ToString();
            Assert.AreEqual(btnResetFiltersText, "Reseteaza filtrele");
        }

        //Test 2 - FlowTests - Should check if CEL.RO offers the posibility to transfer a wishlist (created in this test)
        //to a basket, in order to see if a user that is not logged-in can have this functionality
        [TestMethod]
        public void Should_transfer_wishlist_to_basket()
        {
            ProductsPage productPage = _mainPage.NavigateToProductsPage();
            ProductDetailsPage productDetailsPage = productPage.NavigateToCompareProducts();
            CompareProductPage compareProductPage = productDetailsPage.NavigateToCompareProduct();
            WishlistPage wishlistPage = compareProductPage.NavigateToWishlist();

            WishlistViewModel wishlistView = new WishlistViewModel()
            {
                WishlistAuthor = "Test author",
                WishlistName = "Wishlist Test 18",
                WishlistComment = "Wishlist commeter 13",
                AdditionalNameIfExists = "test"
            };

            BasketShopPage basketShopPage = wishlistPage.CreateNewWishlist(wishlistView);

            Assert.IsTrue(basketShopPage.BasketArrivalConfirmation.Text.ToString().Contains("Continut cos"));
        }

        //Test 3 - FlowTests - Should check if a new review can be added to a product by a non-signed-in user
        [TestMethod]
        public void Should_add_review_to_a_product()
        {
            ProductsPage productPage = _mainPage.NavigateToProductsPage();
            ProductDetailsPage productDetailsPage = productPage.NavigateToProductDetails();

            productDetailsPage.AddReviewProduct(new ReviewViewModel()
            {
                CompleteName = "Vasile Bahoi",
                Email = "yisir52831@animex98.com",
                NoOfStars = 4,
                Comment = "Test review 2"
            });

            string sentReviewConfirmation = productDetailsPage.ConfirmationSent.Text.ToString();
            Assert.IsTrue(sentReviewConfirmation.Equals("Comentariul a fost trimis !"));
        }

        //Test 4 - FlowTests - Should check if CEL.ro offers the posibilities to select a product or to view a set of products
        //based on some elements like a search-bar (universal in app) and some filter elements (Stock, Price, Seller etc) - Version 2

        //Replace the filter criterias if needed, based on what is on CEL.ro products page
        //In this test there was three criterias set, but one another can be added for testing
        [TestMethod]
        public void Should_search_product_by_filtersV2()
        {
            var productPage = _mainPage.SearchProductBySearchBar("laptop");
            productPage.ApplySearchCriteriaV2("pret", new ViewModels.Product.FilterProductsViewModel()
            {
                FirstCriteriaFilter = "producator",
                FirstCriteriaItemNo = 1,
                SecondCriteriaFilter = "stoc",
                SecondCriteriaItemNo = 1,
                ThirdCriteriaFilter = "7fd67",
                ThirdCriteriaItemNo = 1
            });

            string btnResetFiltersText = productPage.BtnResetFilters.Text.ToString();
            Assert.AreEqual(btnResetFiltersText, "Reseteaza filtrele");
        }


        [TestCleanup]
        public void TestCleanup()
        {
            _webDriver.Quit();
        }
    }
}
