using LabProject.Helpers;
using LabProject.PageObjects;
using LabProject.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace LabProject.Tests
{
    [TestClass]
    public class AccountTests
    {
        private IWebDriver _webDriver;
        private LoginPage _loginPage;

        [TestInitialize]
        public void TestInitialisation()
        {
            _webDriver = new ChromeDriver();
            _loginPage = new LoginPage(_webDriver);

            _webDriver.Manage().Window.Maximize();
            _webDriver.Navigate().GoToUrl("https://www.cel.ro/");
        }

        //Test realizat de: Golea Gabriel, impreuna cu Dalnicenco Elizaveta si Rusu Raluca-Elena
        //Test 1 - ACCOUNT - This test should check if a user can succesfully sign in into app (CEL.ro)
        [TestMethod]
        public void Should_login_succesfully()
        {
            LoginViewModel loginView = new LoginViewModel()
            {
                Email = "gabriel.golea18@gmail.com",
                Password = "aFjscIWRDy8c"
            };

            MainPage mainPage = _loginPage.LoginInApplication(loginView);
            mainPage.SlideToProfile();

            Assert.AreEqual(mainPage.UsernameConfirmation.Text.ToString(), "Golea Gabriel");

        }

        //Test realizat de: Golea Gabriel, impreuna cu Dalnicenco Elizaveta si Rusu Raluca-Elena
        //Test 2 - ACCOUNT - This test should check if a user can sign-in with an invalid pair of credentials
        [TestMethod]
        public void Should_not_be_logged_in_app()
        {
            LoginViewModel loginView = new LoginViewModel()
            {
                Email = "abc@gmail.com",
                Password = "Pa55@word1@31"
            };

            _loginPage.LoginInApplication(loginView);

            WaitHelper.WaitForElementVisibility(_webDriver, _loginPage.LblWrongAccountConfirmation);
            Assert.AreEqual(_loginPage.TxtWrongAccountConfirmation.Text.ToString(), "Eroare: Date incorecte.");
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _webDriver.Quit();
        }
    }
}
