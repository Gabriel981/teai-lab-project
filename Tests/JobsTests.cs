﻿using LabProject.Helpers;
using LabProject.PageObjects.Jobs_Module_;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace LabProject.Tests
{
    [TestClass]
    public class JobsTests
    {
        private IWebDriver _webDriver;
        private JobsPage jobsPage;

        [TestInitialize]
        public void TestInitialization()
        {
            _webDriver = new ChromeDriver();
            _webDriver.Manage().Window.Maximize();
            _webDriver.Navigate().GoToUrl("https://jobs.cel.ro/");
            jobsPage = new JobsPage(_webDriver);
        }

        //Test 1 - JobsTests - Shoudl check if a new application to a job was succesfully sent to the CEL.ro team
        //by an anonymous user, that has attached a file with his CV (in pdf format) to the form.
        [TestMethod]
        public void Should_succesfully_post_application()
        {
            JobDetailsPage jobDetailsPage = jobsPage.NavigateToJobDetailsPage();

            // #The path needs to be changed in order to correspond to computer desktop and file
            string FilePathCustom = "‪C:\\Users\\Gabriel\\Desktop\\CV - TEST.pdf";

            JobsPage jobsPageConfirmation = jobDetailsPage.SendApplicationToJob(new PageObjects.Jobs_Module_.Input.JobApplicationViewModel()
            {
                CompleteName = "Vasile Ion",
                EmailAddress = "vasile.ion@mail.com",
                PhoneNumber = "0728289193",
                City = "Oradea",
                Message = "We're sorry. This is just an automated test",
                FilePath = FilePathCustom
            });

            string SuccessConfirmationMessage = jobsPageConfirmation.ConfirmationJobApplication.Text.ToString();
            Assert.IsTrue(condition: SuccessConfirmationMessage.Contains("Iti multumim pentru aplicatie."));
        }

        //Test 2 - FlowTests- Should check if an anonymous user can recommend a job based on e-mail address
        [TestMethod]
        public void Should_recommend_job()
        {
            JobDetailsPage jobDetailsPage = jobsPage.NavigateToJobDetailsPage();
            jobDetailsPage.SendRecommandation("ionut.vasile@mail.com", "iobut.vasile@mail.com");

            WaitHelper.WaitForElementVisibility(_webDriver, jobDetailsPage.PositiveConfirmationText);

            string ConfirmationText = jobDetailsPage.PositiveConfirmation.Text.ToString();

            Assert.AreEqual(expected: "Email has been sent.", actual: ConfirmationText);
        }
        
        //Test 3 - FlowTests - Shoudl check if a non-correctly job recommandation can be sent to the receiver
        //based on the fact that one or more than one fields are wrong(empty, non-valid)
        [TestMethod]
        public void Should_not_set_recommandation_job()
        {
            JobDetailsPage jobDetailsPage = jobsPage.NavigateToJobDetailsPage();
            jobDetailsPage.SendRecommandation("", "iobut.vasile@mail.com");

            WaitHelper.WaitForElementVisibility(_webDriver, jobDetailsPage.NegativeConfirmationText);

            string ErrorMessageConfirmation = jobDetailsPage.NegativeConfirmation.Text.ToString();

            Assert.AreEqual(expected: "Please, fill in all the fields in a valid format.", actual: ErrorMessageConfirmation);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _webDriver.Quit();
        }
    }
}
