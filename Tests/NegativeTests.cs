﻿using LabProject.PageObjects;
using LabProject.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabProject.Tests
{
    [TestClass]
    public class NegativeTests
    {
        private IWebDriver _webDriver;
        private MainPage _mainPage;

        [TestInitialize]
        public void TestInitialization()
        {
            _webDriver = new ChromeDriver();
            _webDriver.Manage().Window.Maximize();
            _webDriver.Navigate().GoToUrl("https://cel.ro");

            _mainPage = new MainPage(_webDriver);
        }

        //Test 1 - NegativeTests - Should check a review can be posted if the minimum length was successfully 
        //achieved, in order to not receive a error
        [TestMethod]
        public void Should_check_minimum_length()
        {
            ProductsPage productsPage = _mainPage.NavigateToProductsPage();
            ProductDetailsPage productDetailsPage = productsPage.NavigateToProductDetails();
            productDetailsPage.AddReviewProduct(new ReviewViewModel()
            {
                CompleteName = "Vasile Bahoi",
                Email = "yisir59931@animex98.com",
                NoOfStars = 4,
                Comment = "Test"
            });


            string Confirmation = productDetailsPage.ConfirmationSent.Text.ToString();
            Assert.IsTrue(Confirmation.Contains("Comentariul trebuie sa aiba 5 caractere !"));
        }

        //Test 2 - NegativeTests - Should check if a numerical value in text input [1] can be passed to the CEL.ro with the review

        // #BUG - The review should not be sent to verifications if property that holds the complete name
        // for review first input is built on numeric characters 
        [TestMethod]
        public void Should_check_name()
        {
            ProductsPage productPage = _mainPage.NavigateToProductsPage();
            ProductDetailsPage productDetailsPage = productPage.NavigateToProductDetails();
            productDetailsPage.AddReviewProduct(new ReviewViewModel()
            {
                CompleteName = "5432524354325",
                Email = "yisir531@animex98.com",
                NoOfStars = 4,
                Comment = "Test565"
            });

            string Confirmation = productDetailsPage.ConfirmationSent.Text;
            Assert.IsTrue(Confirmation.Contains("Comentariul a fost"));
        }

        //Test 3 - NegativeTests - Should check if an alpha-numeric name and a non-valid form will be sent to the CEL.ro
        //in order to be analysed by operators

        // #BUG - The review should not be sent to verifications if property that holds the complete name
        // for review first input is built on alpha-numeric chars.
        [TestMethod]
        public void Should_check_alpha_numeric()
        {
            ProductsPage productPage = _mainPage.NavigateToProductsPage();
            ProductDetailsPage productDetailsPage = productPage.NavigateToProductDetails();
            productDetailsPage.AddReviewProduct(new ReviewViewModel()
            {
                CompleteName = "!!!!!!!!!!//!",
                Email = "yisir531@animex98.com",
                NoOfStars = 4,
                Comment = "...!!!"
            });

            string Confirmation = productDetailsPage.ConfirmationSent.Text.ToString();
            Assert.IsTrue(Confirmation.Contains("Comentariul a fost"));
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _webDriver.Quit();
        }
    }
}
