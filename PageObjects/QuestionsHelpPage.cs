﻿using LabProject.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace LabProject.PageObjects
{
    public class QuestionsHelpPage
    {
        private readonly IWebDriver _webDriver;

        public QuestionsHelpPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        private readonly By SelectBoxQuestion = By.Id("mainSelect");
        public IWebElement SelectQuestion => _webDriver.FindElement(SelectBoxQuestion);
    }
}
