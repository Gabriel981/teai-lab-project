﻿using LabProject.Helpers;
using LabProject.ViewModels;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace LabProject.PageObjects.ContactHelp
{
    public class ContactPage
    {
        private IWebDriver _webDriver;

        public ContactPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        private readonly By ComboQuestionType = By.Id("mainSelect");
        public IWebElement QuestionType => _webDriver.FindElement(ComboQuestionType);

        private readonly By FirstButtonContinue = By.XPath("(//div[@class='content']//div[@class='nextStep'])[4]");
        public IWebElement FirstContinue => _webDriver.FindElement(FirstButtonContinue);

        private readonly By InputGeneralQuestionName = By.Id("generala_nume");
        public IWebElement GeneralQuestionName => _webDriver.FindElement(InputGeneralQuestionName);

        private readonly By InputGeneralQuestionEmail = By.Id("generala_email");
        public IWebElement GeneralQuestionEmail => _webDriver.FindElement(InputGeneralQuestionEmail);

        private readonly By InputGeneralQuestionPhone = By.Id("generala_telefon");
        public IWebElement EtGeneralaPhone => _webDriver.FindElement(InputGeneralQuestionPhone);

        private readonly By SecondButtonContinue = By.CssSelector("#formular-contact > div.contentWrapper > div > div:nth-child(2) > div:nth-child(2) > div.content > div.nextStep");
        public IWebElement BtnContinueXII => _webDriver.FindElement(SecondButtonContinue);

        private readonly By InputGeneralQuestionMessage = By.Id("generala_mesaj");
        public IWebElement GeneralQuestionMessage => _webDriver.FindElement(InputGeneralQuestionMessage);

        private readonly By ButtonSendQuestion = By.XPath("(//div[@class='content']//div[contains (text(), 'Trimite')])[3]");
        public IWebElement SendQuestion => _webDriver.FindElement(ButtonSendQuestion);

        private readonly By LblConfirmationText = By.ClassName("success");
        public IWebElement TxtConfirmation => _webDriver.FindElement(LblConfirmationText);

        public void SendOrderQuestion(QuestionViewModel questionView)
        {
            WaitHelper.WaitForElementVisibility(_webDriver, ComboQuestionType);
            SelectElement dropDownQuestionTypes = new SelectElement(QuestionType);
            dropDownQuestionTypes.SelectByValue(questionView.QuestionType);

            WaitHelper.WaitForElementVisibility(_webDriver, InputGeneralQuestionName);
            GeneralQuestionName.SendKeys(questionView.CompleteName);
            GeneralQuestionEmail.SendKeys(questionView.Email);
            EtGeneralaPhone.SendKeys(questionView.PhoneNumber);

            FirstContinue.Click();

            WaitHelper.WaitForElementVisibility(_webDriver, InputGeneralQuestionMessage);
            GeneralQuestionMessage.SendKeys(questionView.QuestionMessage);

            WaitHelper.WaitForElementVisibility(_webDriver, ButtonSendQuestion, 150);
            SendQuestion.Click();

            WaitHelper.WaitForElementVisibility(_webDriver, LblConfirmationText);
        }
    }
}
