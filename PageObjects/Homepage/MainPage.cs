using LabProject.Helpers;
using LabProject.PageObjects.Account;
using LabProject.PageObjects.ContactHelp;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace LabProject.PageObjects
{
    public class MainPage
    {
        private readonly IWebDriver _webDriver;
        private Actions _actions;

        public MainPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
            _actions = new Actions(_webDriver);
        }

        private readonly By AccountIconImage = By.XPath("//div[@class='top_menu']//a[@class='iconUser']//i");
        public IWebElement AccountIconWrapper => _webDriver.FindElement(AccountIconImage);

        private readonly By ProductCategorySelection = By.XPath("//div[@class='parentCateg'][1]//a[@class='parentCategName']");
        public IWebElement ProductsCategory => _webDriver.FindElement(ProductCategorySelection);
        
        private readonly By ProductSubcategorySelection = By.XPath("//div[@class='subcatWrapper']//a[contains (text(), 'Laptop laptopuri')]");
        private IWebElement ProductsSubcategory => _webDriver.FindElement(ProductSubcategorySelection);

        private readonly By SearchbarInput = By.Id("keyword");
        private IWebElement Searchbar => _webDriver.FindElement(SearchbarInput);

        private readonly By AccountIconMainPage = By.ClassName("icon-icon_user");
        public IWebElement AccountOption => _webDriver.FindElement(AccountIconMainPage);

        private readonly By NewAccountButton = By.XPath("//*[@id='login_header2']/div/a[2]");
        private IWebElement NewAccount => _webDriver.FindElement(NewAccountButton);

        private readonly By PersonalAccountMenuOption = By.XPath("//div[@class='myAccountMenu']//a[contains (text(), 'Contul meu')]");
        public IWebElement PersonalAccountOption => _webDriver.FindElement(PersonalAccountMenuOption);

        private readonly By ButtonContact = By.XPath("//div[@class='menu']//a[@class='contact']");
        public IWebElement ContactButton => _webDriver.FindElement(ButtonContact);

        private readonly By LblUsernameConfirmation = By.XPath("//div[@class='myAccountMenu']//following::div[@class='welcomeMsg']");
        public IWebElement UsernameConfirmation => _webDriver.FindElement(LblUsernameConfirmation);

        public readonly By LblWrongAccountConfirmation = By.ClassName("//span[@class='eroareafis']");
        public IWebElement TxtWrongAccountConfirmation => _webDriver.FindElement(LblWrongAccountConfirmation);

        public readonly By AllProductsCategories = By.Id("MenuLeft");
        public IWebElement ProductCategories => _webDriver.FindElement(AllProductsCategories);

        public ProductsPage NavigateToProductsPage()
        {
            Actions actions = new Actions(_webDriver);

            WaitHelper.WaitForElementVisibility(_webDriver, ProductCategorySelection);
            actions.MoveToElement(ProductsCategory).Perform();

            WaitHelper.WaitForElementVisibility(_webDriver, ProductSubcategorySelection);
            actions.MoveToElement(ProductsSubcategory).Click().Perform();

            return new ProductsPage(_webDriver);
        }

        public ProductsPage SearchProductBySearchBar(string sentKeys)
        {
            Searchbar.Click();
            Searchbar.SendKeys(sentKeys);

            Searchbar.SendKeys(Keys.Return);

            return new ProductsPage(_webDriver);
        }

        public UserProfilePage NavigateToProfile()
        {
            WaitHelper.WaitForElementVisibility(_webDriver, AccountIconImage, 1500);
            _actions.MoveToElement(AccountOption).Perform();

            //Thread.Sleep(1500);
            WaitHelper.WaitForElementVisibility(_webDriver, PersonalAccountMenuOption, 1500);
            PersonalAccountOption.Click();

            return new UserProfilePage(_webDriver);
        } 

        public ContactPage NavigateToContact()
        {
            ContactButton.Click();

            return new ContactPage(_webDriver);
        }

        public RegisterPage NavigateToRegisterPage()
        {
            WaitHelper.WaitForElementVisibility(_webDriver, AccountIconImage);
            _actions.MoveToElement(AccountOption).Perform();

            WaitHelper.WaitForElementVisibility(_webDriver, NewAccountButton);
            _actions.MoveToElement(NewAccount).Click().Perform();

            return new RegisterPage(_webDriver);
        }

        public void SlideToProfile()
        {
            Actions actions = new Actions(_webDriver);
            actions.MoveToElement(AccountIconWrapper).Perform();

            WaitHelper.WaitForElementVisibility(_webDriver, LblUsernameConfirmation);
        }
    }
}
