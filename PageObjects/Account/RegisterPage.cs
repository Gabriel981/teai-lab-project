﻿using LabProject.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LabProject.PageObjects.Account
{
    public class RegisterPage
    {
        private readonly IWebDriver _webDriver;

        public RegisterPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        private By InputEmailAddress = By.XPath("//input[@name='email_address']");
        public IWebElement EmailAddress => _webDriver.FindElement(InputEmailAddress);

        private readonly By ButtonRegister = By.XPath("//button[@type='submit' and @class='btn btn-special']");
        private IWebElement Register => _webDriver.FindElement(ButtonRegister);

        private readonly By NewClientButton = By.XPath("//span[contains(text(),'Sunt client nou')]");
        private IWebElement NewClient => _webDriver.FindElement(NewClientButton);

        public readonly By LblConfirmationError = By.XPath("//label[@class='mesaj_eroare' and @for='email_address']");
        public IList<IWebElement> ConfirmationError => _webDriver.FindElements(LblConfirmationError);

        public void CheckEmailInputLength(string email)
        {
            Actions actions = new Actions(_webDriver);
            actions.MoveToElement(NewClient).Click().Perform();

            EmailAddress.SendKeys(email);

            Register.Click();

            //WaitHelper.WaitForElementVisibility(_webDriver, LblConfirmationError);
        }
    }
}
