﻿using LabProject.Helpers;
using LabProject.ViewModels;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LabProject.PageObjects.Account
{
    public class UserProfilePage
    {
        private IWebDriver _webDriver;

        public UserProfilePage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        private readonly By ButtonShowAllAddresses = By.CssSelector("div#adrese_de_livrare > div.box-content > a.btn-view-all");
        private IWebElement BtnShowAllAddresses => _webDriver.FindElement(ButtonShowAllAddresses);

        private readonly By ButtonAddNewAddress = By.XPath("//div[@class='infoAreaWrapper open']//div[@class='buttons']//a[@class='btn-special']");
        private IWebElement AddAddressNew => _webDriver.FindElement(ButtonAddNewAddress);

        //Popup form new address
        private readonly By InputAddress = By.Id("adresaInput");
        private IWebElement Address => _webDriver.FindElement(InputAddress);

        private readonly By ComboSelectJudet = By.Id("judetadr");
        private IWebElement SelectJudet => _webDriver.FindElement(ComboSelectJudet);

        private readonly By InputLocalitateCombo = By.CssSelector("#account > span.select2-container.select2-container--default.select2-container--open > span.select2-dropdown.select2-dropdown--above > span.select2-search.select2-search--dropdown > input");
        private IWebElement SearchCity => _webDriver.FindElement(InputLocalitateCombo);

        private readonly By ComboSelectCity = By.Id("localitateadr");
        private IWebElement SelectCity => _webDriver.FindElement(ComboSelectCity);

        private readonly By ComboSelectCityOpen = By.CssSelector("span.select2-dropdown.select2-dropdown--above > span.select2-results > ul > li");
        private IList<IWebElement> SelectCityOpen => _webDriver.FindElements(ComboSelectCityOpen);

        private readonly By InputPostalCode = By.Id("codpostal");
        private IWebElement PostalCode => _webDriver.FindElement(InputPostalCode);

        private readonly By ButtonAddAddress = By.XPath("//div[@id='footer_m']//a[@class='btn btn-orange pull-right']");
        private IWebElement AddAdress => _webDriver.FindElement(ButtonAddAddress);

        private readonly By ButtonBackToProfile = By.XPath("//div[@class='infoAreaWrapper open']//div[@class='titlebar']//div[@class='close']//a//span");
        private IWebElement BackToProfile => _webDriver.FindElement(ButtonBackToProfile);

        private readonly By ButtonChangeProfileDetails = By.XPath("//div[@class='header']//a[@class='edit']");
        private IWebElement ChangeProfileDetails => _webDriver.FindElement(ButtonChangeProfileDetails);

        private readonly By InputPhoneNumber = By.Id("telephone");
        private IWebElement PhoneNumberProfile => _webDriver.FindElement(InputPhoneNumber);

        private readonly By ButtonUpdateProfileDetails = By.LinkText("Actualizeaza");
        private IWebElement UpdateProfileDetailsButton => _webDriver.FindElement(ButtonUpdateProfileDetails);

        private readonly By ButtonBackToProfileFromUpdate = By.XPath("(//div[@class='titlebar']//div[@class='close']//a)[3]");
        private IWebElement BackToProfileFromUpdate => _webDriver.FindElement(ButtonBackToProfileFromUpdate);

        private readonly By LblConfirmationProfile = By.CssSelector("#bodycode > div.breadCrumb > span > span:nth-child(3) > span.text");
        public IWebElement ConfirmationProfile => _webDriver.FindElement(LblConfirmationProfile);

        private readonly By SliderNewsletter = By.ClassName("slider");
        private IWebElement Newsletter => _webDriver.FindElement(SliderNewsletter);

        private readonly By LblConfirmationAddresses = By.CssSelector("#adrese_de_livrare > div > a");
        public IWebElement ConfirmationAddresses => _webDriver.FindElement(LblConfirmationAddresses);

        private readonly By ButtonConfirmationNoNewsletter = By.XPath("//div[@id='mesaj_custom']//div[@id='footer_m']//a[@class='btn btn-orange pull-right']");
        public IWebElement ConfirmationNoNewsletter => _webDriver.FindElement(ButtonConfirmationNoNewsletter);

        private readonly By ButtonGoBackNewsletter = By.XPath("(//div[@class='titlebar']//div[@class='close']//a/span[text()= 'Inapoi'])[9]");
        public IWebElement GoBackNewsletter => _webDriver.FindElement(ButtonGoBackNewsletter);

        public void AddNewAddress(ProfileAddressViewModel profileAddressView)
        {
            BtnShowAllAddresses.Click();

            WaitHelper.WaitForElementVisibility(_webDriver, ButtonAddNewAddress);
            AddAddressNew.Click();

            WaitHelper.WaitForElementVisibility(_webDriver, InputAddress, 250);
            Address.SendKeys(profileAddressView.Address);
            SelectElement dropDownJudet = new SelectElement(SelectJudet);

            dropDownJudet.SelectByValue(profileAddressView.Judet);

            SelectCity.Click();
            //SelectCity.SendKeys(profileAddressView.Localitate);
            //SelectElement dropDownLocalitate = new SelectElement(SelectCity);
            //dropDownLocalitate.SelectByText(profileAddressView.Localitate);

            foreach(var localitate in SelectCityOpen)
            {
                if(localitate.Text.ToString() ==  profileAddressView.Localitate)
                {
                    break;
                    localitate.Click();
                }
            }

            PostalCode.SendKeys(profileAddressView.PostalCode);

            WaitHelper.WaitForElementVisibility(_webDriver, ButtonAddNewAddress);
            AddAdress.Click();

            WaitHelper.WaitForElementVisibility(_webDriver, ButtonBackToProfile);
            BackToProfile.Click();

            WaitHelper.WaitForElementVisibility(_webDriver, LblConfirmationAddresses);
        }

        public void UpdateProfileDetails(ProfileDetailsViewModel profileDetailsView)
        {
            WaitHelper.WaitForElementVisibility(_webDriver, SliderNewsletter);
            Newsletter.Click();

            if(ConfirmationNoNewsletter.Displayed == true)
            {
                ConfirmationNoNewsletter.Click();
                GoBackNewsletter.Click();
            }

            WaitHelper.WaitForElementVisibility(_webDriver, ButtonChangeProfileDetails);
            ChangeProfileDetails.Click();

            WaitHelper.WaitForElementVisibility(_webDriver, InputPhoneNumber);
            PhoneNumberProfile.Clear();
            PhoneNumberProfile.SendKeys(profileDetailsView.PhoneNumber);
            UpdateProfileDetailsButton.Click();

            WaitHelper.WaitForElementVisibility(_webDriver, ButtonBackToProfileFromUpdate);

            BackToProfileFromUpdate.Click();
        }

    }
}
