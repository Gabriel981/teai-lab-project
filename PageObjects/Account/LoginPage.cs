﻿using LabProject.Helpers;
using LabProject.ViewModels;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LabProject.PageObjects
{
    public class LoginPage
    {
        private readonly IWebDriver _webDriver;

        public LoginPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        private readonly By AccountIconImage = By.XPath("//div[@class='top_menu']//a[@class='iconUser']//i");
        public IWebElement AccountIconWrapper => _webDriver.FindElement(AccountIconImage);

        private readonly By SecondOptionLoginAccount = By.XPath("//login2[@id='login_header2']//div[@class='myAccountMenu']//a[text()='Conecteaza-te']");
        public IWebElement SecondOptionMenuAccount => _webDriver.FindElement(SecondOptionLoginAccount);

        private readonly By InputEmailAddress = By.XPath("//input[@id='email_address' and @name='email_addressx']");
        public IWebElement EmailAddressLogin => _webDriver.FindElement(InputEmailAddress);

        private readonly By InputPassword = By.XPath("//input[@type='password' and @name='passwordx']");
        public IWebElement PasswordLogin => _webDriver.FindElement(InputPassword);



        private readonly By ButtonLoginForm = By.XPath("//div[@id='logintable']//following::button[@class='btn btn-special btnSubmit']");
        private IWebElement LoginButton => _webDriver.FindElement(ButtonLoginForm);


        public readonly By LblWrongAccountConfirmation = By.XPath("//td[@class='messageStackError box']//span[@class='eroareafis']");
        public IWebElement TxtWrongAccountConfirmation => _webDriver.FindElement(LblWrongAccountConfirmation);


        public MainPage LoginInApplication(LoginViewModel loginView)
        {
            Actions action = new Actions(_webDriver);
            action.MoveToElement(AccountIconWrapper).Perform();

            WaitHelper.WaitForElementVisibility(_webDriver, SecondOptionLoginAccount);
            action.MoveToElement(SecondOptionMenuAccount).Click().Perform();

            WaitHelper.WaitForElementVisibility(_webDriver, InputEmailAddress);
            EmailAddressLogin.SendKeys(loginView.Email);

            PasswordLogin.SendKeys(loginView.Password);
            LoginButton.Click();

            return new MainPage(_webDriver);
        }
    }
}
