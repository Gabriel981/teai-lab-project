﻿using LabProject.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabProject.PageObjects.ApplicationOptions
{
    public class ProductsComparatorPage
    {
        private readonly IWebDriver _webDriver;

        public ProductsComparatorPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public readonly By LblConfirmationComparation = By.XPath("//span[@class='text' and @itemprop='name']");
        public IWebElement Confirmation => _webDriver.FindElement(LblConfirmationComparation);    
    }
}
