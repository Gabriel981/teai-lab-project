﻿using LabProject.Helpers;
using LabProject.ViewModels;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabProject.PageObjects
{
    public class WishlistPage
    {
        private readonly IWebDriver _webDriver;

        public WishlistPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        private By WishlistInput = By.CssSelector("#bodycode > div.pageWrapper.wishlistPageWrapper > div.wishlistWrapper > div > div.buttons > div > span");
        //Elements
        public IWebElement InputLinkWishlist => _webDriver.FindElement(WishlistInput);

        private By InputWishlistName = By.Id("wishlist_name");
        //Wishlist creator
        private IWebElement EdtWishlistName => _webDriver.FindElement(InputWishlistName);

        private By InputWishlistAuthor = By.Id("nume_autor");
        private IWebElement EdtWishlistAuthor => _webDriver.FindElement(InputWishlistAuthor);

        private By InputWishlistComment = By.Id("coment");
        private IWebElement EdtWishlistComment => _webDriver.FindElement(InputWishlistComment);

        private By ButtonCreateWishlist = By.CssSelector("#bodycode > div.pageWrapper.wishlistPageWrapper > form > div.productsListWrapper > div.submitWrapper > input");
        private IWebElement BtnCreateWishlist => _webDriver.FindElement(ButtonCreateWishlist);

        private By ButtonMoveBasket = By.XPath("//div[@class='btn']//following::a[@class='btn-special']");
        private IWebElement BtnMoveToBasket => _webDriver.FindElement(ButtonMoveBasket);

        private By LblWishlistExists = By.XPath("//table[@class='feedbackTable']//tbody//tr//td//span[@class='eroareafis']");
        private IList<IWebElement> ErrorExistsResult => _webDriver.FindElements(LblWishlistExists);

        public BasketShopPage CreateNewWishlist(WishlistViewModel wishlistView)
        {
            WaitHelper.WaitForElementVisibility(_webDriver, InputWishlistName);
            EdtWishlistName.SendKeys(wishlistView.WishlistName);

            EdtWishlistAuthor.Clear();
            EdtWishlistAuthor.SendKeys(wishlistView.WishlistAuthor);

            EdtWishlistComment.SendKeys(wishlistView.WishlistComment);

            BtnCreateWishlist.Click();

            if(ErrorExistsResult.Count() > 0)
            {
                EdtWishlistName.Clear();
                EdtWishlistName.SendKeys(wishlistView.WishlistName + " " + wishlistView.AdditionalNameIfExists);
            }

            BtnMoveToBasket.Click();

            return new BasketShopPage(_webDriver);
        }

    }
}
