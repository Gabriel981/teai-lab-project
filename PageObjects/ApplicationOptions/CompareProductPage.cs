﻿using LabProject.Helpers;
using LabProject.PageObjects.ApplicationOptions;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabProject.PageObjects
{
    public class CompareProductPage
    {
        private readonly IWebDriver _webDriver;

        public CompareProductPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        private readonly By CheckboxAllComparation = By.XPath("//td[@class='tr_label selectAll']//input");
        private IWebElement SelectAllProducts => _webDriver.FindElement(CheckboxAllComparation);

        private readonly By BtnCreate = By.CssSelector("#listform > div.buttons > button:nth-child(4) > i");
        private IWebElement BtnCreateWishlist => _webDriver.FindElement(BtnCreate);

        private readonly By BtnCompare = By.XPath("(//div[@class='buttons']//button[@type='submit'])[1]");
        private IWebElement BtnCompareProducts => _webDriver.FindElement(BtnCompare);

        private readonly By NumberOfProducts = By.Id("prod_ret_cate");
        private IWebElement NumberOfProductsToCompare => _webDriver.FindElement(NumberOfProducts);


        public WishlistPage NavigateToWishlist()
        {
            Actions actions = new Actions(_webDriver);

            WaitHelper.WaitForElementVisibility(_webDriver, CheckboxAllComparation, 200);

            actions.MoveToElement(SelectAllProducts).Click().Perform();

            WaitHelper.WaitForElementVisibility(_webDriver, BtnCreate);

            BtnCreateWishlist.Click();

            return new WishlistPage(_webDriver);
        }

        public ProductsComparatorPage CompareProducts()
        {
            Actions actions = new Actions(_webDriver);

            WaitHelper.WaitForElementVisibility(_webDriver, CheckboxAllComparation);

            actions.MoveToElement(SelectAllProducts).Click().Perform();

            BtnCompareProducts.Click();

            _webDriver.SwitchTo().Window(_webDriver.WindowHandles.Last());

            return new ProductsComparatorPage(_webDriver);
        }

        public bool VerifyNumberOfProducts()
        {
            String numberOfProducts = NumberOfProductsToCompare.Text;
            bool result = false;

            switch (numberOfProducts)
            {
                case "0":
                    result = false;
                    break;
                case "1":
                    result = false;
                    break;
                case "2":
                    result = true;
                    break;
                case "4":
                    result = true;
                    break;
                case "5":
                    result = true;
                    break;
                case "6":
                    result = true;
                    break;
                default:
                    Console.WriteLine("No match found");
                    break;
            }

            return result;

        }


    }
}
