﻿using LabProject.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LabProject.PageObjects
{
    public class BasketShopPage
    {
        private readonly IWebDriver _webDriver;

        public BasketShopPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        private readonly By LblCartPageSummary = By.XPath("//div[@class='mainTotal']//div[@class='sumar']//div");
        public IWebElement CartPageSummary => _webDriver.FindElement(LblCartPageSummary);

        private readonly By SaveWishlist = By.ClassName("btn salveazaWishlist");   
        public IWebElement BtnAddBasketWishlist => _webDriver.FindElement(SaveWishlist);

        private readonly By BasketConfirmation = By.CssSelector("#bodycode > div > span > span:nth-child(3) > span.text");
        public IWebElement BasketArrivalConfirmation => _webDriver.FindElement(BasketConfirmation);

        public bool VerifyProductExistence()
        {
            WaitHelper.WaitForElementVisibility(_webDriver, LblCartPageSummary, 250);

            if(CartPageSummary.Text.ToString() == "Sumar comanda")
            {
                return true;
            }

            return false;
        }

        public void TransferBasketToWishlist()
        {
            BtnAddBasketWishlist.Click();
            WaitHelper.WaitForElementVisibility(_webDriver, BasketConfirmation);
        }
    }
}
