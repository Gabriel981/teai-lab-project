﻿using LabProject.Helpers;
using LabProject.ViewModels;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace LabProject.PageObjects
{
    public class ProductDetailsPage
    {
        private readonly IWebDriver _webDriver;

        private int starNumbers = 0;

        public ProductDetailsPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        private readonly By ButtonCompare = By.XPath("//span[@id='retinut']//a");
        public IWebElement Compare => _webDriver.FindElement(ButtonCompare);

        private readonly By ButtonFavouritesTop = By.Id("retinute");
        public IWebElement FavouritesBtnMenuTop => _webDriver.FindElement(ButtonFavouritesTop);

        private readonly By ButtonAddReviewProduct = By.XPath("//div[@class='topArea']//a[@id='review_default']");
        private IWebElement AddReviewProductButton => _webDriver.FindElement(ButtonAddReviewProduct);

        private readonly By InputReviewName = By.Id("from_name");
        private IWebElement ReviewName => _webDriver.FindElement(InputReviewName);

        private readonly By InputReviewEmail = By.Id("from_email_address");
        private IWebElement ReviewEmail => _webDriver.FindElement(InputReviewEmail);

        private IWebElement StarReviewRating => _webDriver.FindElement(By.CssSelector($"#starify > label:nth-child({starNumbers})"));

        private readonly By InputReviewComment = By.Id("content1");
        private IWebElement ReviewComment => _webDriver.FindElement(InputReviewComment);

        private readonly By ButtonAddReview = By.Name("trimite");
        private IWebElement AddReviewNew => _webDriver.FindElement(ButtonAddReview);

        private readonly By LblReviewSentConfirmation = By.Id("scrie_text");
        public IWebElement ConfirmationSent => _webDriver.FindElement(LblReviewSentConfirmation);

        //Navigation elements
        private readonly By NavigateToProducts = By.CssSelector("#container > div.breadCrumb > span > span:nth-child(5) > a > b");
        private IWebElement NavigateProducts => _webDriver.FindElement(NavigateToProducts);



        public bool VerifyPresence()
        {
            WaitHelper.WaitForElementVisibility(_webDriver, ButtonCompare);
            if (Compare.Text.ToString() == "Compari")
            {
                return true;
            }

            return false;
        }

        public CompareProductPage NavigateToCompareProduct()
        {
            WaitHelper.WaitForElementVisibility(_webDriver, ButtonCompare, 100);
            Compare.Click();

            FavouritesBtnMenuTop.Click();

            return new CompareProductPage(_webDriver);
        }

        public void AddReviewProduct(ReviewViewModel reviewViewModel)
        {
            this.starNumbers = reviewViewModel.NoOfStars;

            WaitHelper.WaitForElementVisibility(_webDriver, ButtonAddReviewProduct, 300);
            AddReviewProductButton.Click();

            WaitHelper.WaitForElementVisibility(_webDriver, InputReviewName, 150);
            ReviewName.SendKeys(reviewViewModel.CompleteName);

            WaitHelper.WaitForElementVisibility(_webDriver, InputReviewEmail);
            ReviewEmail.SendKeys(reviewViewModel.Email);

            WaitHelper.WaitForElementVisibilityV2(_webDriver, StarReviewRating);
            StarReviewRating.Click();

            WaitHelper.WaitForElementVisibility(_webDriver, InputReviewComment);
            ReviewComment.SendKeys(reviewViewModel.Comment);

            WaitHelper.WaitForElementVisibility(_webDriver, ButtonAddReviewProduct);
            AddReviewNew.Click();

            WaitHelper.WaitForElementVisibility(_webDriver, LblReviewSentConfirmation);
        }

        public ProductsPage CompareActionFirstProduct()
        {
            WaitHelper.WaitForElementVisibility(_webDriver, ButtonCompare);
            Compare.Click();

            NavigateProducts.Click();

            return new ProductsPage(_webDriver);
        }

    }
}