﻿using LabProject.Helpers;
using LabProject.ViewModels.Product;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Threading;

namespace LabProject.PageObjects
{
    public class ProductsPage
    {
        private readonly IWebDriver _webDriver;
        private readonly WebDriverWait _webDriverWait;

        public ProductsPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
            _webDriverWait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(15));
        }

        private readonly By SelectedFirstProduct = By.XPath("(//h2[@class='productTitle']//descendant::a[1])[1]");
        public IWebElement FirstProduct => _webDriver.FindElement(SelectedFirstProduct);

        private readonly By SelectedSecondProduct = By.XPath("(//h2[@class='productTitle']//descendant::a[1])[2]");
        public IWebElement SecondProduct => _webDriver.FindElement(SelectedSecondProduct);

        private readonly By ButtonAddBasket = By.XPath("(//div[@cat_nam='Laptop laptopuri']//following::a//span[contains (text(), 'Adauga in cos')])[1]");
        public IWebElement AddToBasket => _webDriver.FindElement(ButtonAddBasket);

        private readonly By FilterProductProducer = By.CssSelector("#filters > div:nth-child(2) > div > div:nth-child(2) > a");
        public IWebElement ProductProducer => _webDriver.FindElement(FilterProductProducer);

        private readonly By FilterProductStock = By.XPath("//div[@class='filter valoare_filtru cln453']//a");
        private IWebElement ProductStock => _webDriver.FindElement(FilterProductStock);

        private readonly By FilterProductSeller = By.XPath("//div[@class='filter valoare_filtru cln708 '][1]//a");
        private IWebElement ProductSeller => _webDriver.FindElement(FilterProductSeller);

        private readonly By ComboOrderCriteria = By.Id("sortare");
        private IWebElement OrderCriteria => _webDriver.FindElement(ComboOrderCriteria);

        private readonly By ButtonResetFilters = By.CssSelector("#bodycode > div.listingPageWrapper > div.listingWrapper > div.filter_container > div.filtreActive > a");
        public IWebElement BtnResetFilters => _webDriver.FindElement(ButtonResetFilters);

        private readonly By ButtonCompareProducts = By.Id("p3314198");
        private IWebElement CompareProducts => _webDriver.FindElement(ButtonCompareProducts);

        private readonly By AllCategoriesFilters = By.Id("filters");
        private IWebElement CategoriesFilters => _webDriver.FindElement(AllCategoriesFilters);


        public ProductDetailsPage NavigateToProductDetails()
        {
            FirstProduct.Click();
            return new ProductDetailsPage(_webDriver);
        }

        public BasketShopPage NavigateToBasket()
        {
            AddToBasket.Click();
            return new BasketShopPage(_webDriver);
        }

        public ProductDetailsPage NavigateToCompareProducts()
        {
            FirstProduct.Click();
            return new ProductDetailsPage(_webDriver);
        }

        //Search based on criterias //v1
        //This type of selection for filters is based on pressing click just on specific items, speciefied by XPath etc.
        public void ApplySearchCriterias(string orderCriteria)
        {
            Actions actions = new Actions(_webDriver);

            WaitHelper.WaitForElementVisibility(_webDriver, FilterProductStock);
            actions.MoveToElement(ProductStock).Click().Perform();

            WaitHelper.WaitForElementVisibility(_webDriver, ComboOrderCriteria);
            OrderCriteria.Click();

            try
            {
                new SelectElement(OrderCriteria).SelectByValue(orderCriteria);
            }
            catch(NoSuchElementException exception)
            {
                System.Diagnostics.Debug.WriteLine(exception.Message.ToString());
            }

            WaitHelper.WaitForElementVisibility(_webDriver, FilterProductSeller);
            ProductSeller.Click();

            WaitHelper.WaitForElementVisibility(_webDriver, ButtonResetFilters);
        }

        //Applying search filters based on what the testers wants to tests in term of filtering products on page
        //This method is based on checking if the input from tester is valid (not null) and also, to select just the 
        //elements that the tester wants (categories + item no. from list).
        public void ApplySearchCriteriaV2(string orderCriteria, FilterProductsViewModel filterProductsView)
        {
            WaitHelper.WaitForElementVisibility(_webDriver, AllCategoriesFilters);

            if(filterProductsView.FirstCriteriaFilter != null)
            {
                var CategoryFilters = CategoriesFilters.FindElements(
                    By.XPath($"//div[@class='d_filtru valori_filtru' and @mkey='{filterProductsView.FirstCriteriaFilter}']//div"));

                for (int i = 0; i < CategoryFilters.Count; i++)
                {
                    if(i == filterProductsView.FirstCriteriaItemNo)
                    {
                        WaitHelper.WaitForElementVisibilityV2(_webDriver, CategoryFilters[i]);
                        CategoryFilters[i].Click();
                    }
                }
            }

            if(filterProductsView.SecondCriteriaFilter != null)
            {
                var CategoryFilters = CategoriesFilters.FindElements(
                    By.XPath($"//div[@class='d_filtru valori_filtru' and @mkey='{filterProductsView.SecondCriteriaFilter}']//div"));

                for (int i = 0; i < CategoryFilters.Count; i++)
                {
                    if (i == filterProductsView.SecondCriteriaItemNo)
                    {
                        WaitHelper.WaitForElementVisibilityV2(_webDriver, CategoryFilters[i]);
                        CategoryFilters[i].Click();
                    }
                }
            }

            if(filterProductsView.ThirdCriteriaFilter != null)
            {
                var CategoryFilters = CategoriesFilters.FindElements(
                 By.XPath($"//div[@class='d_filtru valori_filtru' and @mkey='{filterProductsView.ThirdCriteriaFilter}']//div"));

                for (int i = 0; i < CategoryFilters.Count; i++)
                {
                    if (i == filterProductsView.ThirdCriteriaItemNo)
                    {
                        WaitHelper.WaitForElementVisibilityV2(_webDriver, CategoryFilters[i]);
                        CategoryFilters[i].Click();
                    }
                }
            }

            if(filterProductsView.FourthCriteriaFilter != null)
            {
                var CategoryFilters = CategoriesFilters.FindElements(By.XPath($"//div[@class='d_filtru valori_filtru' and @mkey='{filterProductsView.FourthCriteriaFilter}']//div"));

                for (int i = 0; i < CategoryFilters.Count; i++)
                {
                    if (i == filterProductsView.FourthCriteriaItemNo)
                    {
                        WaitHelper.WaitForElementVisibilityV2(_webDriver, CategoryFilters[i]);
                        CategoryFilters[i].Click();
                    }
                }
            }

            WaitHelper.WaitForElementVisibility(_webDriver, ComboOrderCriteria);
            OrderCriteria.Click();

            try
            {
                new SelectElement(OrderCriteria).SelectByValue(orderCriteria);
            }
            catch (NoSuchElementException exception)
            {
                System.Diagnostics.Debug.WriteLine(exception.Message.ToString());
            }

            WaitHelper.WaitForElementVisibility(_webDriver, ButtonResetFilters);
        }

        public ProductDetailsPage NavigateToCompareProductTwo()
        {
            SecondProduct.Click();
            return new ProductDetailsPage(_webDriver);
        }
    }
}