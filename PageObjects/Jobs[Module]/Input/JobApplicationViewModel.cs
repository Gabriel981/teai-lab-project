﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabProject.PageObjects.Jobs_Module_.Input
{
    public class JobApplicationViewModel
    {
        public string CompleteName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string City { get; set; }
        public string Message { get; set; }
        public string FilePath { get; set; }
    }
}
