﻿using LabProject.Helpers;
using LabProject.PageObjects.Jobs_Module_.Input;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabProject.PageObjects.Jobs_Module_
{
    public class JobDetailsPage
    {
        private IWebDriver _webDriver;

        public JobDetailsPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        private readonly By GotItButton = By.XPath("//a[@class='cc_btn cc_btn_accept_all']");
        public IWebElement GotIt => _webDriver.FindElement(GotItButton);

        private readonly By ApplyJobButton = By.CssSelector("a[data-target='#applyModal']");
        public IList<IWebElement> ApplyNow => _webDriver.FindElements(ApplyJobButton);

        private readonly By InputCompleteName = By.Id("apply_name");
        public IWebElement CompleteName => _webDriver.FindElement(InputCompleteName);

        private readonly By InputEmailAddress = By.Id("apply_email");
        public IWebElement EmailAddress => _webDriver.FindElement(InputEmailAddress);

        private readonly By InputPhoneNumber = By.Id("apply_phone");
        public IWebElement PhoneNumber => _webDriver.FindElement(InputPhoneNumber);

        private readonly By InputLocation = By.Id("apply_location");
        public IWebElement Location => _webDriver.FindElement(InputLocation);

        private readonly By InputMessage = By.Id("apply_msg");
        public IWebElement Message => _webDriver.FindElement(InputMessage);

        private readonly By LblCV = By.Id("cvLabel");
        public IWebElement CVUpload => _webDriver.FindElement(LblCV);

        private readonly By InputCVFile = By.XPath("//div[@class='form-group']//input[@id='cv']");
        public IWebElement CVFile => _webDriver.FindElement(InputCVFile);

        private readonly By SendApplicationButtons = By.XPath("(//button[@class='btn btn-default btn-success mbtn'])[1]");
        public IWebElement SendApplication => _webDriver.FindElement(SendApplicationButtons);

        private readonly By EmailOptionButton = By.XPath("(//li[@title='Drop your friend an email with this job offer.']//a[@data-target='#emailModal'])[2]");
        public IWebElement EmailButton => _webDriver.FindElement(EmailOptionButton);

        private readonly By EmailFormInputRecommandation = By.Id("emailfrom");
        public IWebElement EmailFormFrom => _webDriver.FindElement(EmailFormInputRecommandation);

        private readonly By EmailFormInputTo = By.Id("emailto");
        public IWebElement EmailFormTo => _webDriver.FindElement(EmailFormInputTo);

        private readonly By SendButtonRecommandation = By.XPath("(//div[@class='modal-footer']//button[@class='btn btn-default btn-success mbtn' and text()='Trimite'])[2]");
        public IWebElement SendRecommandationBtn => _webDriver.FindElement(SendButtonRecommandation);

        public readonly By NegativeConfirmationText = By.Id("email-empty");
        public IWebElement NegativeConfirmation => _webDriver.FindElement(NegativeConfirmationText);

        public readonly By PositiveConfirmationText = By.Id("email-positive");
        public IWebElement PositiveConfirmation => _webDriver.FindElement(PositiveConfirmationText);

        public JobsPage SendApplicationToJob(JobApplicationViewModel jobApplicationView)
        {
            WaitHelper.WaitForElementVisibility(_webDriver, GotItButton);
            GotIt.Click();

            Actions actions = new Actions(_webDriver);
            actions.MoveToElement(ApplyNow[1]).Click().Perform();
            
            WaitHelper.WaitForElementVisibility(_webDriver, InputCompleteName);
            CompleteName.SendKeys(jobApplicationView.CompleteName);
            EmailAddress.SendKeys(jobApplicationView.EmailAddress);
            PhoneNumber.SendKeys(jobApplicationView.PhoneNumber);
            Location.SendKeys(jobApplicationView.City);
            Message.SendKeys(jobApplicationView.Message);

            IJavaScriptExecutor jsExecutor = (IJavaScriptExecutor) _webDriver;
            jsExecutor.ExecuteScript("document.getElementById('cv').setAttribute('type', 'text');");

            CVFile.SendKeys(jobApplicationView.FilePath);
            SendApplication.Click();

            return new JobsPage(_webDriver);
        }

        public void SendRecommandation(string FromEmail, string ToEmail)
        {
            EmailButton.Click();

            WaitHelper.WaitForElementVisibility(_webDriver, EmailFormInputRecommandation);
            EmailFormFrom.SendKeys(FromEmail);
            EmailFormTo.SendKeys(ToEmail);

            SendRecommandationBtn.Click();
        }
    }
}
