﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabProject.PageObjects.Jobs_Module_
{
    public class JobsPage
    {
        private readonly IWebDriver _webDriver;

        public JobsPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        private readonly By JobsLabel = By.ClassName("listing-title");
        public IList<IWebElement> JobsSelection => _webDriver.FindElements(JobsLabel);

        private readonly By LblConfirmationJobApplication = By.ClassName("positive-feedback-bracket");
        public IWebElement ConfirmationJobApplication => _webDriver.FindElement(LblConfirmationJobApplication);

        public JobDetailsPage NavigateToJobDetailsPage()
        {
            JobsSelection[1].Click();
            return new JobDetailsPage(_webDriver);
        }
    }
}
